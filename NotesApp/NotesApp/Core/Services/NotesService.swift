//
//  NoteService.swift
//  NotesApp
//
//  Created by Rostyk on 05.04.19.
//  Copyright © 2019 Rostyk. All rights reserved.
//

import RxCocoa
import RxSwift
import Firebase

final class NotesService {
    
    let notes = BehaviorRelay<[Note]>(value: [])
    
    private let ref = Database.database().reference(withPath: "notes")
    
    func observeNotes() {
        ref.observe(.value, with: { [weak self] snapshot in
            var newItems: [Note] = []
            for child in snapshot.children {
                if let snapshot = child as? DataSnapshot,
                    let noteItem = Note(snapshot: snapshot) {
                    newItems.append(noteItem)
                }
            }
            self?.notes.accept(newItems)
        })
    }
    
    func addNote(note: Note) {
        let noteItemRef = self.ref.child(note.id)
        noteItemRef.setValue(note.toAnyObject())
    }
    
    func update(note: Note) {
        let noteItemRef = self.ref.child(note.id)
        noteItemRef.setValue(note.toAnyObject())
    }

}
