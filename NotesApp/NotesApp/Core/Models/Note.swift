//
//  Note.swift
//  NotesApp
//
//  Created by Rostyk on 05.04.19.
//  Copyright © 2019 Rostyk. All rights reserved.
//

import Foundation
import Firebase

struct Note {
    
    var id: String
    var title: String
    var content: String?
    
    func toAnyObject() -> Any {
        return [
            "title": title,
            "content": content
        ]
    }
    
}

extension Note {
    
    init?(snapshot: DataSnapshot) {
        guard let value = snapshot.value as? [String: AnyObject],
            let title = value["title"] as? String else {
                return nil
        }
        
        self.id = snapshot.key
        self.title = title
        self.content = value["content"] as? String
    }
    
}
