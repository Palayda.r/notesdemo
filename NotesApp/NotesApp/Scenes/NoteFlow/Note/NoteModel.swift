//
//  NoteModel.swift
//  NotesApp
//
//  Created by Rostyk on 05.04.19.
//  Copyright © 2019 Rostyk. All rights reserved.
//

import Foundation

class NoteModel: DisposeBagApplicable {
    
    private let notesService: NotesService
    var note: Note
    
    init(notesService: NotesService, note: Note) {
        self.notesService = notesService
        self.note = note
    }
    
    func updateNote(with text: String) {
        note.content = text
        notesService.update(note: note)
    }
    
}
