//
//  NoteViewController.swift
//  NotesApp
//
//  Created by Rostyk on 05.04.19.
//  Copyright © 2019 Rostyk. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class NoteViewController: BaseViewController, DisposeBagApplicable {
    
    private let noteView = NoteView()
    private let model: NoteModel
    
    private var isBulletFlag = false
    private var isDashedFlag = false
    private let bulletPoint: String = "  \u{2022}"
    
    init(with model: NoteModel) {
        self.model = model
        
        super.init()
    }
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialViewSetup()
        setupBindings()
    }
    
    private func initialViewSetup() {
        view.addSubview(noteView)
        noteView.layout {
            $0.leading.equal(to: view.leadingAnchor)
            $0.trailing.equal(to: view.trailingAnchor)
            $0.top.equal(to: view.topAnchor)
            $0.bottom.equal(to: view.bottomAnchor)
        }
        noteView.textView.becomeFirstResponder()
        noteView.textView.delegate = self
        noteView.textView.text = model.note.content
    }
    
    // MARK: - Bindings
    private func setupBindings() {
        noteView.dismissButton.rx.tap.subscribe(onNext: { [weak self] _ in
            guard let self = self else { return }
            
            self.view.endEditing(true)
            self.noteView.textView.isEditable = false
        }).disposed(by: disposeBag)
        
        noteView.textViewTapGesture.rx.event.subscribe(onNext: { [weak self] event in
            guard let self = self else { return }
            
            self.noteView.textView.isEditable = true
            self.noteView.textView.becomeFirstResponder()
        }).disposed(by: disposeBag)
    }
    
    private func checkCloseDashedList(textView: UITextView) -> Bool {
        if textView.text.suffix(1) == "-" {
            deleteLastCharacters(textView: textView, count: 3)
            isDashedFlag = false
            return true
        }
        return false
    }
    
    private func checkCloseBulletList(textView: UITextView) -> Bool {
        if textView.text.suffix(1) == "\u{2022}" {
            deleteLastCharacters(textView: textView, count: 3)
            isBulletFlag = false
            return true
        }
        return false
    }
    
    private func deleteLastCharacters(textView: UITextView, count: Int) {
        for _ in 0..<count {
            guard textView.text.isEmpty else { return }
            textView.text.removeLast()
        }
    }
    
}

extension NoteViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        model.updateNote(with: textView.text)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "" {
            if checkCloseBulletList(textView: textView) || checkCloseDashedList(textView: textView) {
                return false
            }
        }
        if text == "\n" {
            if checkCloseBulletList(textView: textView) || checkCloseDashedList(textView: textView) {
                return false
            }
            if isBulletFlag {
                textView.text.append("\n\(bulletPoint)")
                return false
            }
            if isDashedFlag {
                textView.text.append("\n  -")
                return false
            }
        }
        if text == " " {
            if textView.text.suffix(2) == "\n*" {
                textView.text.removeLast()
                textView.text.append(bulletPoint)
                isBulletFlag = true
                return false
            }
            if textView.text.suffix(2) == "\n-" {
                textView.text.removeLast()
                textView.text.append("  -")
                isDashedFlag = true
                return false
            }
        }
        
        return true
    }
}
