//
//  NoteFlowCoordinator.swift
//  NotesApp
//
//  Created by Rostyk on 05.04.19.
//  Copyright © 2019 Rostyk. All rights reserved.
//

import UIKit

final class NoteFlowCoordinator: FlowCoordinator {
    
    weak var containerViewController: UIViewController?
    
    // TODO: - Swinject
    private let notesService = NotesService()

    func createFlow() -> UIViewController {
        let model = NoteListModel(notesService: notesService)
        // TODO: - Use Event approach
        model.openNote = { note in
            self.openNote(note: note)
        }
        let viewController = NoteListViewController(with: model)
        
        return viewController
    }
    
    // MARK: - Event handling
    
    private func openNote(note: Note) {
        let model = NoteModel(notesService: notesService, note: note)
        let viewController = NoteViewController(with: model)
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
}
