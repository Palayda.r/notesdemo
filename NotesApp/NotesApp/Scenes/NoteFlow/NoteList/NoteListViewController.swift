//
//  NotesListViewController.swift
//  NotesApp
//
//  Created by Rostyk on 05.04.19.
//  Copyright © 2019 Rostyk. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class NoteListViewController: BaseViewController, DisposeBagApplicable {
    
    private let noteListView = NoteListView()
    private let model: NoteListModel
    
    init(with model: NoteListModel) {
        self.model = model
        
        super.init()
    }
    
    // MARK: - View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialViewSetup()
        setupRightNavigationButton()
        configureTableView()
        setupBindings()
    }
    
    // MARK: - Setup
    
    private func initialViewSetup() {
        view.addSubview(noteListView)
        noteListView.layout {
            $0.leading.equal(to: view.leadingAnchor)
            $0.trailing.equal(to: view.trailingAnchor)
            $0.top.equal(to: view.topAnchor)
            $0.bottom.equal(to: view.bottomAnchor)
        }
    }
    
    private func configureTableView() {
        noteListView.tableView.registerReusableCell(cellType: NoteCell.self)
        noteListView.tableView.delegate = self
        noteListView.tableView.dataSource = self
    }
    
    private func setupRightNavigationButton() {
        let rightNavButton = UIBarButtonItem(
            barButtonSystemItem: .add,
            target: self,
            action: #selector(addButtonPressed)
        )
        navigationItem.rightBarButtonItem = rightNavButton
    }
    
    @objc
    private func addButtonPressed() {
        let alert = UIAlertController(
            title: "Add new note",
            message: "",
            preferredStyle: .alert
        )
        let saveAction = UIAlertAction(title: "Add", style: .default) { _ in
            guard let textField = alert.textFields?.first,
                let text = textField.text else { return }
            
            self.model.addNote(title: text)
        }
        
        let cancelAction = UIAlertAction(
            title: "Cancel",
            style: .cancel
        )
        
        alert.addTextField()
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
    }
    
    private func setupBindings() {
        model.notes.asObservable().subscribe(onNext: { [weak self] _ in
            self?.noteListView.tableView.reloadData()
        }).disposed(by: disposeBag)
    }

}

extension NoteListViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.notes.value.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(indexPath, cellType: NoteCell.self)
        cell.configure(title: model.notes.value[indexPath.row].title)
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        model.openNote(at: indexPath.row)
    }

}

