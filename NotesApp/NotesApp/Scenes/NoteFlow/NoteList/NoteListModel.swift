//
//  NoteListModel.swift
//  NotesApp
//
//  Created by Rostyk on 05.04.19.
//  Copyright © 2019 Rostyk. All rights reserved.
//

import RxCocoa
import RxSwift

final class NoteListModel: DisposeBagApplicable {
    
    let notes = BehaviorRelay<[Note]>(value: [])
    
    private let notesService: NotesService
    
    var openNote: ((Note) -> Void)?
    
    init(notesService: NotesService) {
        self.notesService = notesService

        setupBindings()
    }
    
    func addNote(title: String) {
        let note = Note(id: UUID().uuidString, title: title, content: nil)
        
        notesService.addNote(note: note)
    }
    
    func openNote(at index: Int) {
        openNote?(notes.value[index])
    }
    
    private func setupBindings() {
        notesService.observeNotes()
        notesService.notes.asObservable().subscribe(onNext: { [weak self] notes in
            self?.notes.accept(notes)
        }).disposed(by: disposeBag)
    }
    
}

