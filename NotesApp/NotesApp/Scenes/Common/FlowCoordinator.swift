//
//  FlowCoordinator.swift
//  TCPA-M
//
//  Created by Rostyk on 1/15/19.
//  Copyright © 2019 Rostyk. All rights reserved.
//

import UIKit

protocol FlowCoordinator: class {
    
    // should be weak in realization
    var containerViewController: UIViewController? { get set }
    
    @discardableResult
    func createFlow() -> UIViewController
    
}

extension FlowCoordinator {
    
    var navigationController: UINavigationController? {
        return containerViewController as? UINavigationController
    }
    
}
