//
//  BaseViewController.swift
//  TCPA-M
//
//  Created by Rostyk on 1/15/19.
//  Copyright © 2019 Rostyk. All rights reserved.
//

import UIKit

struct ViewControllerAppearanceStrategy {
    
    var shouldShowBackButtonTitle = false
    
}

/// Base UIViewController for all screens used in this module
public class BaseViewController: UIViewController {
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("Init is not implemented")
    }
    
}
