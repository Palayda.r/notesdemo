//
//  DisposeBagApplicable.swift
//  TCPA-M
//
//  Created by Rostyk on 1/25/19.
//  Copyright © 2019 Rostyk. All rights reserved.
//

import RxSwift

protocol DisposeBagApplicable: class {
    
    var disposeBag: DisposeBag { get set }
    
}

private struct AssociatedKeys {
    
    static var disposeBag = "rx_disposeBag"
    
}

extension DisposeBagApplicable {
    
    var disposeBag: DisposeBag {
        get {
            if let disposeBag = objc_getAssociatedObject(self, &AssociatedKeys.disposeBag) as? DisposeBag {
                return disposeBag
            }
            objc_setAssociatedObject(self, &AssociatedKeys.disposeBag, DisposeBag(), .OBJC_ASSOCIATION_RETAIN)
            return self.disposeBag
        }
        set {
            objc_setAssociatedObject(self, &AssociatedKeys.disposeBag, newValue, .OBJC_ASSOCIATION_RETAIN)
        }
    }
    
}
