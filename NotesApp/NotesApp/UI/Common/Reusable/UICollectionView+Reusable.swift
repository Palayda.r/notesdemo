//
//  UICollectionView+Reusable.swift
//  TCPA-M
//
//  Created by Rostyk on 2/5/19.
//  Copyright © 2019 Rostyk. All rights reserved.
//

import UIKit

extension UICollectionView {
    
    // MARK: - UICollectionViewCell
    
    /// Register a Class-Based `UITableViewCell` subclass (conforming to `Reusable`)
    public final func registerReusableCell<T: UICollectionViewCell>(cellType: T.Type) where T: Reusable {
        register(cellType.self, forCellWithReuseIdentifier: cellType.reuseIdentifier)
    }
    
    /// Returns a reusable `UITableViewCell` object for the class inferred by the return-type
    public final func dequeueReusableCell<T: UICollectionViewCell>(_ indexPath: IndexPath,
                                                                   cellType: T.Type = T.self) -> T
        where T: Reusable {
            guard let cell = dequeueReusableCell(
                withReuseIdentifier: cellType.reuseIdentifier,
                for: indexPath
                ) as? T else {
                    fatalError(
                        """
                        Failed to dequeue a cell with identifier \(cellType.reuseIdentifier)
                        matching type \(cellType.self).
                        Check that the reuseIdentifier is set properly and that you registered the cell beforehand.
                        """
                    )
            }
            
            return cell
    }
    
    // MARK: - UICollectionReusableView
    
    /// Register a Class-Based `UICollectionReusableView` subclass (conforming to `Reusable`) as a Supplementary View
    public final func registerReusableSupplementaryView<T: UICollectionReusableView>(elementKind: String,
                                                                                     viewType: T.Type)
        where T: Reusable {
            register(
                viewType.self,
                forSupplementaryViewOfKind: elementKind,
                withReuseIdentifier: viewType.reuseIdentifier
            )
    }
    
    /// Returns a reusable `UICollectionReusableView` object for the class inferred by the return-type
    public final func dequeueReusableSupplementaryView<T: UICollectionReusableView>
        (elementKind: String, indexPath: IndexPath, viewType: T.Type = T.self) -> T where T: Reusable {
        guard let view = dequeueReusableSupplementaryView(
            ofKind: elementKind,
            withReuseIdentifier: viewType.reuseIdentifier,
            for: indexPath
            ) as? T else {
                fatalError(
                    """
                    Failed to dequeue a supplementary view with identifier \(viewType.reuseIdentifier)
                    matching type \(viewType.self).
                    Check that the reuseIdentifier is set properly and that you registered
                    the supplementary view beforehand.
                    """
                )
        }
        
        return view
    }
    
}
