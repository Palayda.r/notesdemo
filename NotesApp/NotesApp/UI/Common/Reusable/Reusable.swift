//
//  Reusable.swift
//  TCPA-M
//
//  Created by Rostyk on 2/5/19.
//  Copyright © 2019 Rostyk. All rights reserved.
//

import UIKit

// Inspired by https://github.com/AliSoftware/Reusable

/// Protocol for `UITableViewCell` and `UICollectionViewCell` subclasses when they are code-based.
/// Conform cells to `Reusable` to be able to dequeue them in a type-safe manner.
public protocol Reusable: class {
    
    static var reuseIdentifier: String { get }
    
}

public extension Reusable {
    
    public static var reuseIdentifier: String {
        return String(describing: self)
    }
    
}
