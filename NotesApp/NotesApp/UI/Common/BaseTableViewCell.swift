//
//  BaseTableViewCell.swift
//  UI
//
//  Created by Rostyk on 2/5/19.
//  Copyright © 2019 Rostyk. All rights reserved.
//

import UIKit

public class BaseTableViewCell: UITableViewCell {
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
