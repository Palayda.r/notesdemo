//
//  BaseTextView.swift
//  UI
//
//  Created by Rostyk on 2/5/19.
//  Copyright © 2019 Rostyk. All rights reserved.
//

import UIKit

class BaseTextView: UITextView {
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("Init is not implemented")
    }
    
}
