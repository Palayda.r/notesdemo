//
//  BaseView.swift
//  UI
//
//  Created by Rostyk on 2/5/19.
//  Copyright © 2019 Rostyk. All rights reserved.
//

import UIKit

/// Base View for all custom views used in this module
public class BaseView: UIView {
    
    public init() {
        super.init(frame: .zero)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("Init is not implemented")
    }
    
}
