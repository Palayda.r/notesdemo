//
//  NoteListView.swift
//  NotesApp
//
//  Created by Rostyk on 05.04.19.
//  Copyright © 2019 Rostyk. All rights reserved.
//

import UIKit

final class NoteListView: BaseView {
    
    let tableView = UITableView()
    
    override init() {
        super.init()
        
        backgroundColor = .white
        configureTableView()
        addTableView()
    }
    
    private func configureTableView() {
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .white
        tableView.rowHeight = 54.0
    }
    
    private func addTableView() {
        addSubview(tableView)
        
        tableView.layout {
            $0.leading.equal(to: leadingAnchor)
            $0.trailing.equal(to: trailingAnchor)
            $0.top.equal(to: topAnchor)
            $0.bottom.equal(to: bottomAnchor)
        }
    }
}

