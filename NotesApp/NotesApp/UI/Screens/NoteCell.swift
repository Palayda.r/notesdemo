//
//  NoteCell.swift
//  NotesApp
//
//  Created by Rostyk on 05.04.19.
//  Copyright © 2019 Rostyk. All rights reserved.
//

import UIKit

public final class NoteCell: BaseTableViewCell, Reusable {
    
    private let titleLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        cellInitialSetup()
        setupView()
    }
    
    func configure(title: String) {
        titleLabel.text = title
    }
    
    private func cellInitialSetup() {
        backgroundColor = .white
    }
    
    private func setupView() {
        setupTitleLabel()
    }
    
    private func setupTitleLabel() {
        titleLabel.font = UIFont.systemFont(ofSize: 16.0)
        titleLabel.minimumScaleFactor = 0.5
        
        addSubview(titleLabel)
        titleLabel.layout {
            $0.leading.equal(to: leadingAnchor, offsetBy: 15.0)
            $0.trailing.equal(to: trailingAnchor, offsetBy: -15.0)
            $0.centerY.equal(to: centerYAnchor)
        }
    }
    
}

