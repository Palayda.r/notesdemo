//
//  NoteView.swift
//  NotesApp
//
//  Created by Rostyk on 05.04.19.
//  Copyright © 2019 Rostyk. All rights reserved.
//

import UIKit

final class NoteView: BaseView {
    
    let textView = UITextView()
    let dismissButton = UIButton()
    let textViewTapGesture = UITapGestureRecognizer()
    
    override init() {
        super.init()
        
        setupTextView()
        addTextView()
        addDismissButton()
    }
    
    private func addTextView() {
        addSubview(textView)
        
        textView.layout {
            $0.leading.equal(to: leadingAnchor)
            $0.trailing.equal(to: trailingAnchor)
            $0.top.equal(to: topAnchor)
            $0.bottom.equal(to: centerYAnchor)
        }
    }
    
    private func addDismissButton() {
        addSubview(dismissButton)
        
        dismissButton.layout {
            $0.leading.equal(to: leadingAnchor)
            $0.trailing.equal(to: trailingAnchor)
            $0.top.equal(to: centerYAnchor)
            $0.bottom.equal(to: bottomAnchor)
        }
        dismissButton.backgroundColor = .red
    }
    
    private func setupTextView() {
        textView.isEditable = true
        textView.isSelectable = true
        textView.dataDetectorTypes = [.link, .phoneNumber]
        textView.font = UIFont.systemFont(ofSize: 17.0)
        textView.addGestureRecognizer(textViewTapGesture)
    }
}
